#!/usr/bin/env bash

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

source "${script_dir}/../lib/utils.bash"

testPMV_Linux_x86() {
  local url
  url=$(get_download_url "pmv" "linux-gnu" "amd64" "3.19.23")
  assertNotNull "Unable to obtain a pmv/linux/x86_64 download url" "$url"
  curl --fail --location "${url}" >/dev/null || {
    fail "Unable to download from ${url}"
  }
}

testPMV_Macos() {
  local url
  url=$(get_download_url "pmv" "darwin23.0" "arm64" "3.19.23")
  assertNotNull "Unable to obtain a pmv/darwin/arm64 download url" "$url"

  curl --fail --location "${url}" >/dev/null || {
    fail "Unable to download from ${url}"
  }
}

testTerraTransformer_Linux_x86() {
  local url
  url=$(get_download_url "terra-transformer" "linux-gnu" "amd64" "1.28.4")
  assertNotNull "Unable to obtain a pmv/linux/x86_64 download url" "$url"

  curl --fail --location "${url}" >/dev/null || {
    fail "Unable to download from ${url}"
  }
}

testTerraTransform_Macos() {
  local url
  url=$(get_download_url "terra-transformer" "darwin23.0" "arm64" "1.28.4")
  assertNotNull "Unable to obtain a pmv/darwin/arm64 download url" "$url"

  curl --fail --location "${url}" >/dev/null || {
    fail "Unable to download from ${url}"
  }
}

testJsonnetTool_Linux_x86() {
  local url
  url=$(get_download_url "jsonnet-tool" "linux-gnu" "amd64" "1.15.7")
  assertNotNull "Unable to obtain a pmv/linux/x86_64 download url" "$url"

  curl --fail --location "${url}" >/dev/null || {
    fail "Unable to download from ${url}"
  }
}

testJsonnetTool_Macos() {
  local url
  url=$(get_download_url "jsonnet-tool" "darwin23.0" "arm64" "1.15.7")
  assertNotNull "Unable to obtain a pmv/darwin/arm64 download url" "$url"

  curl --fail --location "${url}" >/dev/null || {
    fail "Unable to download from ${url}"
  }
}

source "${script_dir}/shUnit2/shUnit2.sh"
