#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

export MISE_VERBOSE=1
MISE_CONFIG_DIR=$(mktemp -d)
export MISE_CONFIG_DIR
export MISE_ENV=test
export MISE_GLOBAL_CONFIG_FILE=${MISE_CONFIG_DIR}/config.toml
export MISE_DATA_DIR=${MISE_CONFIG_DIR}/data

touch "${MISE_GLOBAL_CONFIG_FILE}"
mkdir -p "${MISE_DATA_DIR}"

GL_INFRA_ASDF_TOOL_NAME=$1

eval "$(mise activate bash)"

mise plugins link "${GL_INFRA_ASDF_TOOL_NAME}" "${script_dir}/.."
mise ls-remote "${GL_INFRA_ASDF_TOOL_NAME}"
mise install "${GL_INFRA_ASDF_TOOL_NAME}@latest"
mise activate
mise use "${GL_INFRA_ASDF_TOOL_NAME}@latest"

ls -laR "$MISE_DATA_DIR/installs/${GL_INFRA_ASDF_TOOL_NAME}/latest/bin"

mise which "${GL_INFRA_ASDF_TOOL_NAME}"

echo "✅ Test for '${GL_INFRA_ASDF_TOOL_NAME}' completed successfully"

# Cleanup
rm -rf "${script_dir}/../.mise.test.toml" "${MISE_CONFIG_DIR}"
