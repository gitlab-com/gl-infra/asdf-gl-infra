#!/usr/bin/env bash

set -euo pipefail

plugin_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

check_installation_type() {
  [[ $ASDF_INSTALL_TYPE == "version" ]] || {
    echo "ASDF_INSTALL_TYPE '${ASDF_INSTALL_TYPE}' not supported"
    exit 1
  } >&2
}

# Detect the tool name from the asdf plugin directory.
get_tool_name() {
  if [[ -n ${GL_INFRA_ASDF_TOOL_NAME-} ]]; then
    echo "${GL_INFRA_ASDF_TOOL_NAME}"
    return
  fi

  basename "$(dirname "${plugin_dir}")"
}

find_download_in_release_json() {
  local arg_release_json=$1
  local arg_tool=$2
  local arg_version=$3
  local arg_architecture=$4
  local arg_operating_system=$5

  echo "Searching for name=${arg_tool}, version=${arg_version} architecture=${arg_architecture}, operating_system=${arg_operating_system}" >&2

  jq -r -n \
    --argjson json "${arg_release_json}" \
    --arg tool "${arg_tool}" \
    --arg version "${arg_version}" \
    --arg arch "${arg_architecture}" \
    --arg ostype "${arg_operating_system}" '
    $json | [
      .assets.links[] |
      select(.name|ascii_downcase == "\($tool|ascii_downcase)_\($version|ascii_downcase)_\($ostype|ascii_downcase)_\($arch|ascii_downcase).tar.gz")
    ] |
    first |
    .direct_asset_url // ""
  '
}

get_project_url() {
  local tool=$1

  echo "https://gitlab.com/api/v4/projects/gitlab-com%2fgl-infra%2f${tool}"
}

# Fetch the most appropriate download URL.
get_download_url() {
  local tool=$1
  local ostype=$2
  local uname_m=$3
  local version=$4

  local project_url
  project_url=$(get_project_url "${tool}")

  local release_url="${project_url}/releases/v${version}"

  release_json=$(curl --silent --fail "${release_url}")
  readonly release_json

  if [[ -z $release_json ]]; then
    echo "Failed to download release information from ${release_url}" >&2
    return 1
  fi

  local operating_system
  local architecture

  # Pick OS.
  if [[ ${ostype} == "linux-"* ]]; then
    operating_system="Linux"
  elif [[ ${ostype} == "darwin"* ]]; then
    operating_system="Darwin"
  fi

  # Pick architecture.
  case "$uname_m" in
  aarch64_be | aarch64 | armv8b | armv8l | arm64)
    architecture="arm64"
    ;;
  *)
    architecture="x86_64"
    ;;
  esac

  download_url="$(find_download_in_release_json "$release_json" "$tool" "$version" "$architecture" "$operating_system")"

  if [[ $architecture == "arm64" ]] && [[ -z $download_url ]]; then
    # Fall back to x86_64 if an arm64 binary is unavailable.
    download_url="$(find_download_in_release_json "$release_json" "$tool" "$version" "x86_64" "$operating_system")"
  fi

  echo "$download_url"
}

# Remove the quarantine flag on macOS.
remove_quarantine_flag() {
  local file=$1

  if [[ $OSTYPE == "darwin"* ]]; then
    (xattr -d com.apple.quarantine "$file" >/dev/null 2>&1) || true
  fi
}

cosign_verification_failed() {
  echo "⚠️ Failed to verify download using cosign: $1" >&2

  # TODO: make cosign verification mandatory
  if [[ -n ${ASDF_GL_INFRA_REQUIRE_VERIFICATION-} ]]; then
    exit 1
  fi
}

verify_cosign_bundle() {
  local arg_tool_name=$1
  local arg_version=$2
  local arg_tarball_path=$3
  local arg_cosign_bundle_path=$4

  if ! [[ -f ${arg_cosign_bundle_path} ]]; then
    # TODO: make cosign verification mandatory
    cosign_verification_failed "no cosign verification bundle was found."
    return
  fi

  if ! command -v cosign &>/dev/null; then
    # TODO: make cosign verification mandatory
    cosign_verification_failed "no cosign binary is available on the PATH."
    return
  fi

  # Extract the cert and the sig from the bundle
  jq -r '.cert' "${arg_cosign_bundle_path}" >"${arg_cosign_bundle_path}.cert"
  jq -r '.base64Signature' "${arg_cosign_bundle_path}" >"${arg_cosign_bundle_path}.sig"

  # Verify the blob
  cosign verify-blob \
    --signature "${arg_cosign_bundle_path}.sig" \
    --certificate "${arg_cosign_bundle_path}.cert" \
    --certificate-oidc-issuer "https://gitlab.com" \
    --certificate-identity "https://gitlab.com/gitlab-com/gl-infra/${arg_tool_name}//.gitlab-ci.yml@refs/tags/v${arg_version}" \
    "${arg_tarball_path}" || {
    # TODO: make cosign verification mandatory
    cosign_verification_failed "cosign verification failed."
    return
  }

  echo "💎 verified ${arg_tool_name} download as authentic" >&2
}
