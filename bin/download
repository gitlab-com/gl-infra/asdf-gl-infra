#!/usr/bin/env bash

set -euo pipefail

plugin_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# shellcheck source=../lib/utils.bash
source "${plugin_dir}/../lib/utils.bash"

check_installation_type

tool_name=$(get_tool_name)

readonly tarball_path="${ASDF_DOWNLOAD_PATH}/${tool_name}-v${ASDF_INSTALL_VERSION}.tar.gz"
readonly cosign_bundle_path="${ASDF_DOWNLOAD_PATH}/${tool_name}-v${ASDF_INSTALL_VERSION}.bundle"

mkdir -p "${ASDF_DOWNLOAD_PATH}"

if [[ $OSTYPE == "linux-"* ]]; then
  operating_system="Linux"
elif [[ $OSTYPE == "darwin"* ]]; then
  operating_system="Darwin"
fi

download_url=$(get_download_url "${tool_name}" "$OSTYPE" "$(uname -m)" "${ASDF_INSTALL_VERSION}")
cosign_bundle_url=${download_url%.tar.gz}.bundle

readonly download_url
readonly cosign_bundle_url

if [[ -z $download_url ]]; then
  echo "❌ Unable to download a suitable version of ${tool_name} for ${operating_system}, $(uname -m) and version ${ASDF_INSTALL_VERSION}" >&2
  exit 1
fi

curl --silent --fail --location -o "${tarball_path}" "${download_url}" || {
  echo "❌ Failed to download the ${tool_name} package bundle from ${download_url}" >&2
  exit 1
}

curl --silent --fail --location -o "${cosign_bundle_path}" "${cosign_bundle_url}" || {
  # TODO: make cosign verification mandatory
  cosign_verification_failed "Failed to download the verification bundle from ${cosign_bundle_url}."
}

remove_quarantine_flag "${tarball_path}"
