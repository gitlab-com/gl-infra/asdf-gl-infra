# asdf-gl-infra

An [asdf](https://github.com/asdf-vm/asdf) plugin for various GitLab Infrastructure tools.

## Installation

* **terra-transformer**: `asdf plugin add terra-transformer https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra.git`

* **pmv**: `asdf plugin add pmv https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra.git`

* **jsonnet-tool**: `asdf plugin add jsonnet-tool https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra.git`

## Keyless Verification

In order to reduce the likelihood of supply chain attacks, this plugin will progressively begin the process of verifying
binaries using the Sigstore projects `cosign` tool, and keyless signatures.

GitLab binaries should apply the `goreleaser` configuration from the
[`common-template-copier`](https://gitlab.com/gitlab-com/gl-infra/common-template-copier/-/blob/main/%7B%25%20if%20golang%20%25%7D.goreleaser.yml%7B%25%20endif%20%25%7D.jinja)
project, in order to ensure that keyless signing bundles are produced for GitLab go binaries.

The verification process uses this signature to ensure that the artifact is authentication, and guarantees that it
was produced in a GitLab CI pipeline on a tag reference.

Given that tag references are protected, this helps to protect against supply-chain attacks on GitLab Infrastructure
internal tooling.

In order to use `cosign` verification, ensure that a copy of `cosign` is available on your path.

Note: during the rollout phase, verification will fail with an warning message only, and proceed to install the binary.
In future, verification will become mandatory. To force verification on, set the environment variable
`ASDF_GL_INFRA_REQUIRE_VERIFICATION`.

## Requires

* `curl`
* `jq`
* `cosign` (optional, for now)
